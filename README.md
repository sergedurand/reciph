# ReCIPH: Relational Coefficients for Input Partitioning Heuristic



ReCIPH is a heuristic used to select the best input to partition when doing neural network verification with reachability analysis based on linear relaxations (e.g using abstract domains [deeppoly](https://www.sri.inf.ethz.ch/publications/singh2019domain) or [deepzono](https://www.sri.inf.ethz.ch/publications/singh2018effective)).

Essentially it amounts to multiplying input width with final coefficients on the outputs associated with each inputs and chosing the input with highest score.

This work as been accepted as a poster in the [1st Workshop on Formal Verification of Machine Learning (WFVML 2022)](https://www.ml-verification.com/), co-located with ICML 2022.

Feel free to check the paper for more details.

Contact: serge.durand2@cea.fr
